package com.example.ws.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author will.tuo
 * @date 2021/10/18 13:34
 * @Description: 编写一个WebSocketConfig配置类，注入对象ServerEndpointExporter， 这个bean会自动注册使用了@ServerEndpoint注解声明的Websocket
 * endpoint
 */
@Configuration
public class WebSocketConfig {

    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
